#ifndef IMAGE_H
#define IMAGE_H

#include <malloc.h>
#include <stdint.h>

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct pixel { uint8_t b, g, r; };

struct image* image_create(const uint64_t width, const uint64_t height);

void image_destroy (struct image* const image);

#endif
