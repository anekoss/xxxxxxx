#ifndef ROTATE_H
#define ROTATE_H

#include "image.h"
#include <stdlib.h>

struct image* rotate(struct image* const source );

#endif
