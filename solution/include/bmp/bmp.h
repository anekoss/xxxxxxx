#ifndef BMP_H
#define BMP_H

#include "../image.h"
#include <stdio.h>

enum read_status {
    READ_OK,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum write_status {
    WRITE_OK,
    WRITE_ERROR
};

enum read_status from_bmp(FILE* const in, struct image* const image);
enum write_status to_bmp(FILE* const out, const struct image* image);

#endif
