#ifndef BMP_PADDING_H
#define BMP_PADDING_H

#include <stdint.h>

uint8_t get_padding(const uint32_t width);

#endif
