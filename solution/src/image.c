#include "../include/image.h"

struct image* image_create(const uint64_t width, const uint64_t height) {
    struct image* image = malloc(sizeof(struct image));
    image->width = width;
    image->height = height;
    image->data = malloc(width * height * sizeof(struct pixel));
    return image;
}

void image_destroy(struct image* const image) {
    free(image->data);
    free(image);
}

