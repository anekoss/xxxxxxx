#include "../include/file_works.h"
#include "../include/bmp/bmp.h"
#include "../include/rotate.h"

#include <stdio.h>

int main( int argc, char** argv ) {
    if (argc != 3 ) {
        fprintf(stderr, "not enough args\n");
        return -1;
    }

    char* const input_file = argv[1];
    char* const output_file= argv[2];
    FILE *input = NULL;
    FILE *output= NULL;
    struct image* image = {0};
    if(!open_file(input, input_file, "r") ){
        fprintf(stderr, "Cannot open file for reading\n");
        return -1;
    }

    enum read_status read_status = from_bmp(input, image);

    if (read_status != READ_OK){
        fprintf(stderr, "Cannot read the input file\n");
        image_destroy(image);
        if (close_file(input)){
            fprintf(stderr, "Can't close the input file\n");
            return -1;
        }
        return -1;
    }

    if(!(open_file(output, output_file, "w") )){
        image_destroy(image);
        if (close_file(input)){
            fprintf(stderr, "Cannot close the input file");
            return -1;
        }
        fprintf( stderr, "Cannot open file for writing");
        return -1;
    }
    struct image* rotated = {0};
    rotated = rotate(image);
    enum write_status write_status = to_bmp(output, rotated);
    image_destroy(image);
    if (write_status != WRITE_OK){
        fprintf(stderr, "Cannot write to the output file");
        image_destroy(rotated);
        return -1;
    }

    if(!close_file(input)){
        fprintf(stderr, "Can't close the input file");
        image_destroy(rotated);
        return -1;
    }

    if(!close_file(output)){
        fprintf(stderr, "Cannot close the output file");
        image_destroy(rotated);
        return -1;
    }
    image_destroy(rotated);
    
    return 0;
}
