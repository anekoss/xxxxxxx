#include "../../include/bmp/bmp.h"
#include "../../include/bmp/bmp_header.h"
#include "../../include/bmp/bmp_padding.h"

static uint32_t find_image_size(const uint64_t width, const uint64_t height) {
    return (width * sizeof(struct pixel) + get_padding(width)) * height;
}

static uint32_t find_file_size(const uint32_t image_size) {
    return image_size + sizeof(struct bmp_header);
}

static struct bmp_header create_header(const uint32_t width, const uint32_t height) {
    const uint32_t  image_size = find_image_size(width, height);
    const uint32_t file_size = find_file_size(image_size);
    return (struct bmp_header) {
            .bfType = 0x4d42,
            .bfileSize = file_size,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = image_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

static enum write_status write_header(FILE* const out, const struct image* image) {
    const struct bmp_header header = create_header(image->width, image->height);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) return WRITE_ERROR;
    return WRITE_OK;
}

static enum write_status write_pixels(FILE *out, const struct image *image) {
    const uint8_t padding = get_padding(image->width);
    const size_t width = image->width;
    const size_t height = image->height;
    const uint8_t paddings[3] = {0};
    for (size_t i = 0; i < height; i++) {
        if ((!fwrite((image->data) + i * width, sizeof(struct pixel) * width, 1, out) ||
            !fwrite(paddings, padding, 1, out)) && padding != 0) return WRITE_ERROR;
    }
    return WRITE_OK;
}

enum write_status to_bmp(FILE* const out, const struct image* image) {
    const enum write_status status = write_header(out, image);
    if (status != WRITE_OK) return status;
    return write_pixels(out, image);
}


