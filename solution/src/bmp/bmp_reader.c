#include "../../include/bmp/bmp.h"
#include "../../include/bmp/bmp_header.h"
#include "../../include/bmp/bmp_padding.h"

#include <malloc.h>
#include <stdio.h>

uint8_t get_padding(const uint32_t width) {
    const uint8_t padding = (width * sizeof(struct pixel)) % 4;
    return padding ? 4 - padding : padding;
}

static enum read_status read_header(FILE* const in, struct bmp_header* const header) {
    if (fread(header, sizeof(struct bmp_header), 1, in) == 1) return READ_OK;
    return READ_INVALID_HEADER;
}

static enum read_status read_pixels(FILE* const f, const struct image* image) {
    const uint32_t width = image->width;
    const uint32_t height = image->height;
    for (size_t i = 0; i < height; i++) {
        if (fread(image->data + i * width, sizeof(struct pixel), width, f ) != width ||
            fseek(f, get_padding(width), SEEK_CUR) != 0) return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum read_status from_bmp(FILE* const in, struct image* const image) {
    struct bmp_header header = {0};
    enum read_status status = read_header(in, &header);
    if(status != READ_OK) return status;
    fseek(in,header.bOffBits,SEEK_SET);
    image->width = header.biWidth;
    image->height = header.biHeight;
    image->data = malloc(sizeof(struct pixel)*image->height*image->width);
    status = read_pixels(in, image);
    if( status != READ_OK) return status;
    return READ_OK;
}








