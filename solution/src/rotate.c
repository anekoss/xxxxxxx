#include "../include/rotate.h"

struct image* rotate(struct image* const source ) {
    struct image* image = image_create(source->width, source->height);
    for (size_t i = 0; i < image->height; i++) {
        for (size_t j = 0; j < image->width; j++) {
            struct pixel pixel = source->data[(image->width - j - 1) * source->width + i];
            image->data[i * image->width + j] = pixel;
        }
    }
    return image;
}
